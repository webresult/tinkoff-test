FROM golang:1.8-jessie

# Configuring GLIBC
RUN echo "deb http://http.us.debian.org/debian/ testing non-free contrib main" >> /etc/apt/sources.list
RUN apt-get update -y && \
    apt-get install curl libc6 pkg-config uuid-runtime -y

ADD ./ /go/src
ENV GOPATH="/go/src/"
RUN go env GOROOT GOPATH

# Building app
RUN go get -u github.com/kardianos/govendor
WORKDIR /go/src/go/src/
RUN /go/src/go/bin/govendor sync
WORKDIR /go/src/go/bin/