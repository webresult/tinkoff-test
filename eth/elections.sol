pragma solidity ^0.4.0;

contract Elections {
    // candidate ids constant list
    int8 constant notCandidate = 3;
    int8 constant firstCandidate = 1;
    int8 constant secondCandidate = 2;
    mapping(int8 => bool) existsCanditates;

    mapping(address => int8) votedClient;
    mapping(int8 => int) canditatesVotes;

    address public owner = msg.sender;

    function Elections() public {
        existsCanditates[notCandidate] = true;
        existsCanditates[firstCandidate] = true;
        existsCanditates[secondCandidate] = true;
    }

    function vote(int8 candidateId) public returns(bool) {
        // check correct candidate id
        require(existsCanditates[candidateId] != bool(false));

        // check duplicate votes
        require(votedClient[msg.sender] == int8(0));

        votedClient[msg.sender] = candidateId;
        canditatesVotes[candidateId] = canditatesVotes[candidateId] + 1;
        return true;
    }

    // return votes count for candidate
    function result(int8 candidateId) public onlyBy(owner) returns (int) {
        return canditatesVotes[candidateId];
    }

    modifier onlyBy(address _account)
    {
        require(msg.sender == _account);
        _;
    }
}