## For all tasks:
### Run
sudo docker-compose up

## First task:

I've decided to use address for client identification. It can be interpreted like passport id in real life.

I've decided to did't develop web interface because we can't to see contract method result.

So I can't to check that contract return about this address vote.

I've tested contract on https://remix.ethereum.org .

You can see contract sourse in eth/elections.sol

I've made simple interface for using eth from application instead web form.

### At first time:
    curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"personal_listAccounts","params":[],"id":1}'  http://0.0.0.0:8545

replace 0xb6ffa56337ee8d585790af5fc954300f8cd4fd9b on real address

    curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"personal_newAccount","params":["password"],"id":1}'  http://0.0.0.0:8545

    curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"personal_unlockAccount","params":["0xb6ffa56337ee8d585790af5fc954300f8cd4fd9b", "password"],"id":1}'  http://0.0.0.0:8545

    curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"miner_setEtherbase","params":["0xb6ffa56337ee8d585790af5fc954300f8cd4fd9b"],"id":1}'  http://0.0.0.0:8545

    curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"miner_start","params":[],"id":1}'  http://0.0.0.0:8545

wait for about 5-10 minutes

    curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"eth_getBalance","params":["0xb6ffa56337ee8d585790af5fc954300f8cd4fd9b", "latest"],"id":1}'  http://0.0.0.0:8545

    curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"personal_unlockAccount","params":["0xb6ffa56337ee8d585790af5fc954300f8cd4fd9b", "password"],"id":1}'  http://0.0.0.0:8545

    curl -H "Content-Type: application/json" -X POST --data '{"jsonrpc":"2.0","method":"eth_sendTransaction","params":[{"from": "0xb6ffa56337ee8d585790af5fc954300f8cd4fd9b", "data": "0x606060405233600360006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff160217905550341561005057600080fd5b5b6001600080600360000b60000b815260200190815260200160002060006101000a81548160ff0219169083151502179055506001600080600160000b60000b815260200190815260200160002060006101000a81548160ff0219169083151502179055506001600080600260000b60000b815260200190815260200160002060006101000a81548160ff0219169083151502179055505b5b61032f806100f86000396000f30060606040526000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff1680634cf87d00146100545780638c402ac51461008e5780638da5cb5b146100cc575b600080fd5b341561005f57600080fd5b610078600480803560000b906020019091905050610121565b6040518082815260200191505060405180910390f35b341561009957600080fd5b6100b2600480803560000b9060200190919050506101a4565b604051808215151515815260200191505060405180910390f35b34156100d757600080fd5b6100df6102dd565b604051808273ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200191505060405180910390f35b6000600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff168073ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff1614151561018057600080fd5b600260008460000b60000b81526020019081526020016000205491505b5b50919050565b60008015156000808460000b60000b815260200190815260200160002060009054906101000a900460ff161515141515156101de57600080fd5b6000800b600160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060009054906101000a900460000b60000b14151561023e57600080fd5b81600160003373ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff16815260200190815260200160002060006101000a81548160ff021916908360000b60ff1602179055506001600260008460000b60000b81526020019081526020016000205401600260008460000b60000b815260200190815260200160002081905550600190505b919050565b600360009054906101000a900473ffffffffffffffffffffffffffffffffffffffff16815600a165627a7a723058209d544b6eef23471db786a9d617bc941fc1b373da2fd9ef74e445b361115d12b00029", "gas": "0x21000"}],"id":1}'  http://0.0.0.0:8545

you will see created contract txid like 0x910bd6f53b2457f4c5e08f59f0fd7791c77b4f9550f286c555c02a526f781976

see it in docker log and you will find contract address

put address, password, contract address to the go/src/elections/main.go and restart docker for test vote

## Second task:
http://localhost:8090


