package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/pkg/errors"
	"encoding/json"
	"strconv"
)

const (
	ExmoUrl = "https://api.exmo.com/v1/"
)

type ExmoApi struct{}

type ExmoTickers map[string] ExmoTicker

type ExmoTicker struct {
	BuyPrice  string `json:"buy_price"`
	SellPrice string `json:"sell_price"`
	LastTrade string `json:"last_trade"`
	High      string
	Low       string
	Avg       string
	Vol       string
	VolCurr   string `json:"vol_curr"`
	Updated   int64
}

func NewExmoApi() ExmoApi {
	return ExmoApi{}
}

func (api *ExmoApi) Tickers() (response ExmoTickers, err error) {
	resp, err := request("GET", ExmoUrl+"ticker", nil)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"resp": string(resp),
		}).Info("Get exmo tickers error")
		return response, errors.New("Get exmo tickers error")
	}
	err = json.Unmarshal(resp, &response)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"resp": string(resp),
		}).Info("Get exmo tickers error")
		return response, errors.New("Unmarshal exmo tickers error")
	}
	return response, nil
}

// use last value to highlight difference between exchanges
// avg value can get small difference
func (t ExmoTicker) GetAvg() (price float64, err error) {
	return strconv.ParseFloat(t.LastTrade, 64)
}

func (t ExmoTickers) CorrectPairTickers() (res ExmoTickers) {
	res = ExmoTickers{}
	for pair, ticker := range t {
		pair = clearPairName(pair)
		res[pair] = ticker
	}
	return
}
