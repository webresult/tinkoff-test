package main

import (
	log "github.com/sirupsen/logrus"
	"sync"
	"time"
	"math"
	tt "html/template"
)

func initWorker() {
	eApi := NewExmoApi()
	bApi := NewBitlishApi()

	for {
		log.Info("Get tickers")
		wg := sync.WaitGroup{}
		wg.Add(2)
		var (
			eTickers   ExmoTickers
			bTickers   BitlishTickers
			eErr, bErr error
		)

		go func() {
			eTickers, eErr = eApi.Tickers()
			log.WithError(eErr).WithFields(log.Fields{
				"tickers": eTickers,
			}).Info("Get exmo tickers")
			wg.Done()
		}()
		//=============================
		go func() {
			bTickers, bErr = bApi.Tickers()
			log.WithError(bErr).WithFields(log.Fields{
				"tickers": bTickers,
			}).Info("Get bitlish tickers")
			wg.Done()
		}()
		//=============================
		wg.Wait()

		if bErr != nil || eErr != nil {
			log.Info("Skip new tickers by error")
			continue
		}
		//=============================
		res := fetchRequiredTickers(eTickers, bTickers)
		log.WithFields(log.Fields{
			"result": res,
		}).Info("Get required tickers")
		tTickers.Lock()
		tTickers.Tickers = res
		tTickers.Unlock()

		timeT := time.Now()
		timeTicker := timeT.Format("new Date(2006, 01, 02, 15, 04, 05)")
		for _, ticker := range res {
			tHistory.Lock()
			if _, ok := tHistory.Tickers[ticker.Pair]; !ok {
				pairRow := ExchangeTickersHistory{
					Min:     -1.0,
					Bitlish: []ExchangeHistoryPrice{},
					Exmo:    []ExchangeHistoryPrice{},
				}
				tHistory.Tickers[ticker.Pair] = pairRow
			}
			lastPair := tHistory.Tickers[ticker.Pair]

			eRow := ExchangeHistoryPrice{T: tt.JS(timeTicker), V: ticker.ExmoPrice, S: timeT}
			lastPair.Exmo = append(lastPair.Exmo, eRow)

			bRow := ExchangeHistoryPrice{T: tt.JS(timeTicker), V: ticker.BitlishPrice, S: timeT}
			lastPair.Bitlish = append(lastPair.Bitlish, bRow)

			if lastPair.Min == -1.0 || ticker.ExmoPrice < lastPair.Min || ticker.BitlishPrice < lastPair.Min {
				if ticker.ExmoPrice < ticker.BitlishPrice {
					lastPair.Min = ticker.ExmoPrice
				} else {
					lastPair.Min = ticker.BitlishPrice
				}
				lastPair.Min = lastPair.Min * 0.99
			}

			tHistory.Tickers[ticker.Pair] = lastPair
			tHistory.Unlock()
		}

		time.Sleep(30 * time.Second)
	}
}

func fetchRequiredTickers(eTickers ExmoTickers, bTickers BitlishTickers) (res []RequiredTicker) {
	exmoTickers := eTickers.CorrectPairTickers()
	for pair, bTicker := range bTickers {
		if eTicker, ok := exmoTickers[pair]; ok {
			bPrice, err := bTicker.GetAvg()
			if err != nil {
				log.WithError(err).WithFields(log.Fields{
					"pair":   pair,
					"ticker": bTicker,
				}).Info("Get avg price for bitlish ticker error")
				continue
			}
			ePrice, err := eTicker.GetAvg()
			if err != nil {
				log.WithError(err).WithFields(log.Fields{
					"pair":   pair,
					"ticker": eTicker,
				}).Info("Get avg price for exmo ticker error")
				continue
			}
			if bPrice == 0.0 || ePrice == 0.0 {
				log.WithError(err).WithFields(log.Fields{
					"pair":   pair,
					"bPrice": bPrice,
					"ePrice": ePrice,
				}).Info("Zero avg price for ticker error")
				continue
			}
			var perc float64
			if bPrice < ePrice {
				perc = ((ePrice - bPrice) / bPrice) * 100
			} else {
				perc = ((bPrice - ePrice) / ePrice) * 100
			}

			if perc > 1 {
				reqTicker := RequiredTicker{Pair: pair, BitlishPrice: bPrice, ExmoPrice: ePrice, Perc: math.Ceil(perc)}
				res = append(res, reqTicker)
			} else {
				log.WithError(err).WithFields(log.Fields{
					"pair":    pair,
					"bTicker": bTicker,
					"ePrice":  ePrice,
					"perc":    perc,
				}).Info("Small difference between prices")
			}
		} else {
			log.WithFields(log.Fields{
				"pair": pair,
			}).Info("Ticker not found in exmo tickers")
		}
	}
	return
}
