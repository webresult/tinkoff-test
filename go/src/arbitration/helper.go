package main

import (
	log "github.com/sirupsen/logrus"
	"net/url"
	"time"
	"github.com/parnurzeal/gorequest"
	"errors"
	"strings"
)

type TickersSortedByTime []ExchangeHistoryPrice

func (a TickersSortedByTime) Len() int           { return len(a) }
func (a TickersSortedByTime) Swap(i, j int)      { a[i], a[j] = a[j], a[i] }
func (a TickersSortedByTime) Less(i, j int) bool { return a[i].S.Before(a[j].S) }

const (
	Timeout = 30
)

func request(method, urlReq string, reqBody url.Values) (respBody []byte, err error) {
	respBody = []byte{}
	err = nil

	tDuration := time.Duration(Timeout)
	timeout := time.Duration(tDuration * time.Second)
	request := gorequest.New().Timeout(timeout)
	request.Client.Timeout = timeout
	switch method {
	case "GET":
		request.Get(urlReq)
	case "POST":
		request.Post(urlReq)
	case "DELETE":
		request.Delete(urlReq)
	case "PUT":
		request.Put(urlReq)
	}

	resp, body, errs := request.Send(reqBody.Encode()).EndBytes()

	if errs == nil {
		stringBody := string(body)
		statusCode := resp.StatusCode
		resp.Body.Close()

		if resp.StatusCode != 200 && resp.StatusCode != 201 {
			log.WithFields(log.Fields{
				"method":     method,
				"statuscode": resp.StatusCode,
				"urlReq":     urlReq,
				"body":       stringBody,
				"statusCode": statusCode,
				"data":       reqBody.Encode(),
			}).Info("Wrong status request")
			return respBody, errors.New("Request error")
		}
		respBody = body
	} else {
		statusCode := 0
		if resp != nil {
			statusCode = resp.StatusCode
		}
		log.WithFields(log.Fields{
			"method":     method,
			"errs":       errs,
			"body":       string(body),
			"statuscode": statusCode,
			"urlReq":     urlReq,
			"statusCode": statusCode,
			"data":       reqBody.Encode(),
		}).Info("Request has errors")
		return respBody, errors.New("Request error")
	}

	return
}

// remove unused chars from pair name
// bring to lowercase
func clearPairName(pair string) string {
	pair = strings.Replace(pair, "_", "", -1)
	pair = strings.ToLower(pair)
	return pair
}
