package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/pkg/errors"
	"encoding/json"
	"strconv"
)

const (
	BitlishUrl = "https://bitlish.com/api/v1/"
)

type BitlishApi struct{}

type BitlishTickers map[string] BitlishTicker

type BitlishTicker struct {
	First string
	Last  string
	Max   string
	Min   string
	Prc   string
	Sum   string
}

func NewBitlishApi() BitlishApi {
	return BitlishApi{}
}

func (api *BitlishApi) Tickers() (response BitlishTickers, err error) {
	resp, err := request("GET", BitlishUrl+"tickers", nil)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"resp": string(resp),
		}).Info("Get Bitlish tickers error")
		return response, errors.New("Get Bitlish tickers error")
	}
	err = json.Unmarshal(resp, &response)
	if err != nil {
		log.WithError(err).WithFields(log.Fields{
			"resp": string(resp),
		}).Info("Get Bitlish tickers error")
		return response, errors.New("Unmarshal Bitlish tickers error")
	}
	return response, nil
}


// use last value to highlight difference between exchanges
// avg value can get small difference
func (t BitlishTicker) GetAvg() (price float64, err error) {
	return strconv.ParseFloat(t.Last, 64)
}