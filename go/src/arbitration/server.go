package main

import (
	"net/http"
	tt "html/template"
	"github.com/gorilla/mux"
	"sort"
)

type ChartPage struct {
	Min            float64
	Pair           string
	ExmoTickers    []ExchangeHistoryPrice
	BitlishTickers []ExchangeHistoryPrice
}

func TickerHandler(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
	t := tt.Must(tt.ParseFiles("/go/tpl/index.html"))
	tTickers.Lock()
	t.Execute(w, tTickers.Tickers)
	tTickers.Unlock()
}

func ChartHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	pair := vars["pair"]
	tHistory.Lock()
	tickers, ok := tHistory.Tickers[pair]
	if !ok {
		tHistory.Unlock()
		w.WriteHeader(http.StatusNotFound)
		return
	}
	w.WriteHeader(http.StatusOK)
	t := tt.Must(tt.ParseFiles("/go/tpl/chart.html"))
	eTickers := TickersSortedByTime(tickers.Exmo)
	sort.Sort(eTickers)
	bTickers := TickersSortedByTime(tickers.Bitlish)
	sort.Sort(bTickers)
	data := ChartPage{ExmoTickers: eTickers, BitlishTickers: bTickers, Pair: pair, Min: tickers.Min}
	t.Execute(w, data)
	tHistory.Unlock()
}
