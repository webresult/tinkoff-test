package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/gorilla/mux"
	"net/http"
	"sync"
	tt "html/template"
	"time"
)

type RequiredTicker struct {
	Pair         string
	Perc         float64
	ExmoPrice    float64
	BitlishPrice float64
}

type TopicalTickersHistory struct {
	sync.Mutex
	Tickers map[string]ExchangeTickersHistory
}

type ExchangeHistoryPrice struct {
	S time.Time
	T tt.JS
	V float64
}

type ExchangeTickersHistory struct {
	Min     float64
	Exmo    []ExchangeHistoryPrice
	Bitlish []ExchangeHistoryPrice
}

type TopicalTickers struct {
	sync.Mutex
	Tickers []RequiredTicker
}

var tTickers TopicalTickers
var tHistory TopicalTickersHistory

func init() {
	tTickers = TopicalTickers{Tickers: []RequiredTicker{}}
	tHistory = TopicalTickersHistory{Tickers: map[string]ExchangeTickersHistory{}}
	go initWorker()
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/", TickerHandler)
	r.HandleFunc("/chart/{pair}", ChartHandler)
	http.Handle("/", r)

	err := http.ListenAndServe(":8080", r)
	if err != nil {
		log.WithError(err).Fatal("ListenAndServer fatal")
	}
}
