package main

import (
	log "github.com/sirupsen/logrus"
	"time"
)

func main()  {
	time.Sleep(10 * time.Second)
	ethereum := &Ethereum{}
	ethereum.Open("http://eth:8545")

	address := "0xb6ffa56337ee8d585790af5fc954300f8cd4fd9b"
	password := "password"
	contract := "0xB8AA8C3A36E603dCB01C0769dB7C80a285efa810"

	result, err := ethereum.Vote("1", address, password, contract)
	log.WithFields(log.Fields{
		"data":  result,
		"err": err,
	}).Info("Ethereum vote")
}