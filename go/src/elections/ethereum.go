package main

import (
	log "github.com/sirupsen/logrus"
	"github.com/ethereum/go-ethereum/rpc"
	"time"
	"github.com/ethereum/go-ethereum/common"
	"errors"
	"regexp"
	"strings"
)

const (
	Name     = "Ethereum"
	// HumanStandard signatures
	SignatureVote     = "0x8c402ac5"
)

type Ethereum struct {
	Client *rpc.Client
	unlockTime map[string]time.Time
	unlocked map[string]bool
}

type StructLog struct {
	Pc         uint64                      `json:"pc"`
	Op         string                      `json:"op"`
	Gas        uint64                      `json:"gas"`
	GasCost    uint64                      `json:"gasCost"`
	Memory     []string                    `json:"memory"`
	MemorySize int                         `json:"memSize"`
	Stack      []string                    `json:"stack"`
	Storage    map[common.Hash]common.Hash `json:"-"`
	Depth      int                         `json:"depth"`
	Err        interface{}                 `json:"error"`
}

// open connection
func (c *Ethereum) Open(host string) error {
	c.unlockTime = make(map[string]time.Time)
	c.unlocked = make(map[string]bool)

	ethereumClient, err := rpc.DialHTTP(host)
	if err != nil {
		log.WithFields(log.Fields{
			"err":  err,
			"host": host,
		}).Errorln("Ethereum open connect err")
		return err
	}

	c.Client = ethereumClient
	log.Infoln("[Coin] Ethereum", host, "connected")
	return err
}

// close connection
func (c *Ethereum) Close() {
	c.Client.Close()
}

// get new password
func (c *Ethereum) GetNewPassword() string {
	return GenerateUuid(32)
}

// unlock account
func (c *Ethereum) PersonalUnlockAccount(address string, password string, period int) (bool, error) {
	if address == "" {
		return false, errors.New("Address is empty")
	}

	var unlocked bool
	err := c.Client.Call(&unlocked, "personal_unlockAccount", address, password, period)
	if err != nil {
		log.WithFields(log.Fields{
			"err":     err,
			"address": address,
			"period":  period,
		}).Error("Ethereum PersonalUnlockAccount err")
	}
	return unlocked, err
}

// checks address is valid
func (c *Ethereum) IsValidAddress(address string) bool {
	validAddressCommon, _ := regexp.Compile("^0x[a-fA-F0-9]{40}$")
	validAddressUpper, _ := regexp.Compile("^0x[A-F0-9]{40}$")
	validAddressLower, _ := regexp.Compile("^0x[a-f0-9]{40}$")
	if !validAddressCommon.MatchString(address) {
		// check if it has the basic requirements of an address
		return false
	}
	if validAddressUpper.MatchString(address) || validAddressLower.MatchString(address) {
		// If it's all small caps or all upper caps, return true
		return true
	}

	//  Checks if the given string is a checksummed address
	// EIP55-compliant hex string representation of the address https://github.com/ethereum/EIPs/issues/55
	return common.HexToAddress(address).Hex() == address
}

// address to save in transaction model
func (Ethereum) AddressToLower(address string) string {
	return strings.ToLower(address)
}

// vote
func (c *Ethereum) Vote(vote string, address, password, contract string) (interface{}, error) {
	c.PersonalUnlockAccount(address, password, 1000)
	data := SignatureVote + LeftPad(vote, 64)
	params := struct {
		From string
		To   string
		Data string
	}{Data: data, From: address, To: contract}
	var result interface{}
	err := c.Client.Call(&result, "eth_call", params, "latest")
	log.WithFields(log.Fields{
		"address": address,
		"params":  params,
	}).Info("Ethereum vote")
	if err != nil {
		log.WithFields(log.Fields{
			"err":     err,
			"address": address,
			"params":  params,
		}).Error("Ethereum vote err")
	}
	return result, err
}
