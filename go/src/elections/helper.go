package main

import (
	"os/exec"
	"fmt"
	"strings"
	"math/big"
	"encoding/hex"
	log "github.com/sirupsen/logrus"
)


// padding left with zero
func LeftPad(input string, length int) string {
	input = strings.Replace(input, "0x", "", -1)
	for i := len(input); i < length; i++ {
		input = "0" + input
	}
	return input
}

// hex to dec
func HexDec(hexStr string) big.Int {
	hexStr = strings.Replace(hexStr, "0x", "", -1)
	result, success := new(big.Int).SetString(hexStr, 16)
	if success {
		return *result
	} else {
		return big.Int{}
	}
}

// dec to hex
func DecHex(intDec *big.Int) string {
	return fmt.Sprintf("0x%x", intDec)
}

// hex to string
func HexString(hexStr string) string {
	hexStr = strings.Replace(hexStr, "0x", "", -1)
	result, err := hex.DecodeString(hexStr)
	if err != nil {
		log.WithFields(log.Fields{
			"err":    err,
			"hexStr": hexStr,
		}).Error("Helper err: HexString")
	}
	return string(result)
}

func GenerateUuid(n int) string {
	output := getUuid()
	attempts := 0
	for len(output) < n && attempts < 10 {
		output = output + getUuid()
		attempts ++
	}

	if len(output) < n {
		return ""
	}
	return output[:n]
}

func getUuid() string {
	out, _ := exec.Command("uuidgen").Output()
	output := fmt.Sprintf("%s", out)
	output = strings.Trim(output, " ")
	output = strings.Trim(output, "\n")
	return output
}
